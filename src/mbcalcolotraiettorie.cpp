/******************************************************************
 *   © 2020 Matteo Bigliardi
 *   email: 239958@studenti.unimore.it
 * 
 *   mbcalcolotraiettorie.cpp
 * 
 ******************************************************************/

#include <nlopt.hpp>
#include <iostream>

#include <Eigen/Dense>

#include <fstream>
#include <chrono>

#include <stdint.h> 
#include <stdio.h>
#include <stdlib.h>


#include <ros/ros.h>
#include "std_msgs/String.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Float32.h"

#include "rosgraph_msgs/Clock.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Twist.h"
#include "geometry_msgs/Pose2D.h"
#include "nav_msgs/Path.h"


//flag per scegliere traiettoria delle diverse scene
int flag_scena=2;


//----DatiPosizione--------------------------------------
Eigen::VectorXd DPxk(2700);
int ScorriDPxk=3;

//-----VarGlobali---------------------------------------------
double pi = 3.14159265358;
std::vector<double> G_posizione_iniziale (3);

std::vector<double> G_vel_precedente {0,0};



//file 
std::ofstream myfile;

//file con x;y di traiettoria
std::ofstream FTraiettoria;

//dichiarazione funzioni per funzione di costo
Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u);
Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts);
double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1);
double FunzioneDiCosto(Eigen::VectorXd du);






int main(int argc, char *argv[])
{
	//printf(".. creo rosnode mbnodecontrollerifopt \n");
  myfile.open ("traiettorie.txt");

  if (flag_scena == 1){
      FTraiettoria.open ("traiettoria_xy_1.csv");
  }
  if (flag_scena == 2){
      FTraiettoria.open ("traiettoria_xy_2.csv");
  }
  
  
  myfile << "rostopic pub /trajectory nav_msgs/Path ";
  myfile << "\"{ poses: [ ";

  if (flag_scena == 1)
  {
    // si presuppone circorferenza percorsa a velocità costante
    DPxk(0) = 0;
    DPxk(1) = 0;
    DPxk(2) = pi / 2;
    DPxk(3) = 0;
    DPxk(4) = 0;
    DPxk(5) = pi / 2;
    DPxk(6) = 0;
    DPxk(7) = 0;
    DPxk(8) = pi / 2;
    DPxk(9) = 0;
    DPxk(10) = 0;
    DPxk(11) = pi / 2;
    DPxk(12) = 0;
    DPxk(13) = 0;
    DPxk(14) = pi / 2;
    DPxk(15) = 0;
    DPxk(16) = 0;
    DPxk(17) = pi / 2;

    

    for (int k = 18; k <= 2160; k = k + 18)
    {
      if (k == 18)
      {
        double work2 = DPxk(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";

        FTraiettoria << std::to_string(DPxk(0)) << ";" << std::to_string(DPxk(1)) << "\n";
      }
      if (k != 18)
      {
        myfile << ",\n";
      }
      if (k>=18 && k<=180)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0, -1.107;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";
      }
       if (k>=198 && k<=1980)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0.167, 0;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
       
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }
      if (k>=1998 )
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0, -0.463;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

      
    }
  }

  if (flag_scena == 2)
  {
    // si presuppone circorferenza percorsa a velocità costante
    DPxk(0) = 0;
    DPxk(1) = 0.5;
    DPxk(2) = pi / 2;
    DPxk(3) = 0;
    DPxk(4) = 0.5;
    DPxk(5) = pi / 2;
    DPxk(6) = 0;
    DPxk(7) = 0.5;
    DPxk(8) = pi / 2;
    DPxk(9) = 0;
    DPxk(10) = 0.5;
    DPxk(11) = pi / 2;
    DPxk(12) = 0;
    DPxk(13) = 0.5;
    DPxk(14) = pi / 2;
    DPxk(15) = 0;
    DPxk(16) = 0.5;
    DPxk(17) = pi / 2;

    
    for (int k = 18; k <=2646; k = k + 18)
    {
      if (k == 18)
      {
        double work2 = DPxk(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk(0)) << ", y: " << std::to_string(DPxk(1)) << " }, orientation: {z: " << std::to_string(-sin(work2)) << "} } }";
        myfile << ",\n";

        FTraiettoria << std::to_string(DPxk(0)) << ";" << std::to_string(DPxk(1)) << "\n";

      }
      if (k != 18)
      {
        myfile << ",\n";
       
      }
      if (k>=18 && k<=180)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0, -pi / 2;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }
      if (k>=198 && k<=1080)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0.15, 0;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

      if (k>=1098 && k<=1260)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0, pi / 2;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

      if (k>=1278 && k<=2160)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0.157, -pi / 5;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

      if (k>=2178 && k<=2340)
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0, pi / 2;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

      if ( k>=2358 )
      {
        Eigen::VectorXd DPuk(2);
        DPuk << 0.15, 0;

        Eigen::VectorXd DP(3);
        DP << DPxk(k - 3), DPxk(k - 2), DPxk(k - 1);
        Eigen::VectorXd DPxk1;
        DPxk1 = mobRobDT0(DP, DPuk, 0.1);
        DPxk(k) = DPxk1(0);
        DPxk(k + 1) = DPxk1(1);
        DPxk(k + 2) = DPxk1(2);
        DPxk(k + 3) = DPxk1(0);
        DPxk(k + 4) = DPxk1(1);
        DPxk(k + 5) = DPxk1(2);
        DPxk(k + 6) = DPxk1(0);
        DPxk(k + 7) = DPxk1(1);
        DPxk(k + 8) = DPxk1(2);
        DPxk(k + 9) = DPxk1(0);
        DPxk(k + 10) = DPxk1(1);
        DPxk(k + 11) = DPxk1(2);
        DPxk(k + 12) = DPxk1(0);
        DPxk(k + 13) = DPxk1(1);
        DPxk(k + 14) = DPxk1(2);
        DPxk(k + 15) = DPxk1(0);
        DPxk(k + 16) = DPxk1(1);
        DPxk(k + 17) = DPxk1(2);
        double work = DPxk1(2) / 2;
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }"<<",\n";
        myfile << "{ pose: { position: {x: " << std::to_string(DPxk1(0)) << ", y: " << std::to_string(DPxk1(1)) << " }, orientation: {z: " << std::to_string(-sin(work)) << "} } }";

        FTraiettoria << std::to_string(DPxk1(0)) << ";" << std::to_string(DPxk1(1)) << "\n";

      }

    }
  }

  myfile << "] }\"\n";


  

  
  
  myfile.close();
  FTraiettoria.close();
  
  return 0;
}

//inizio funzione di costo

   Eigen::VectorXd mobRobCT0(Eigen::VectorXd x, Eigen::VectorXd u)
    {
      //Continuous-time nonlinear model of a differential drive mobile robot

      // 3 states (x):
      //   robot x position (x)
      //   robot y position (y)
      //  robot yaw angle (theta)
      //2 inputs (u):
      //linear velocity (v_r)
      //angular velocity(w_r)
      double theta_r = x(2);
      Eigen::VectorXd dxdt = x;
      double v_r = u(0);
      double w_r = u(1);
      dxdt(0) = cos(theta_r) * v_r;
      dxdt(1) = sin(theta_r) * v_r;
      dxdt(2) = w_r;
      Eigen::VectorXd y = x;

      return dxdt;
    };

    // ----wrapToPi--------------------------------------------------------
    double wrapToPi(double x){
    x = fmod(x + pi,2*pi);
    if (x < 0)
        x += 2*pi;
    return x - pi;
    };

    // -------------mobRobDT0----------------------------------------------
    Eigen::VectorXd mobRobDT0(Eigen::VectorXd xk, Eigen::VectorXd uk, double Ts)
    {
      int M = 10;
      double delta = Ts / M;
      Eigen::VectorXd xk1 = xk;
      for (int i = 0; i < M; i++)
      {
        xk1 = xk1 + delta*mobRobCT0(xk1,uk); 
        
      }
      //wraptopi
     
      xk1(2)= wrapToPi(xk1(2));
      Eigen::VectorXd yk = xk;

      return xk1;
    };

    // ----mobRobCostFCN---------------------------------------------------
    double mobRobCostFCN(Eigen::VectorXd du, Eigen::VectorXd x0, double Ts, int N, int M, Eigen::MatrixXd xref, Eigen::VectorXd u1)
    {
      /* Cost function of nonlinear MPC for mobile robot control

 Inputs:
   du:      optimization variable, from time k to time k+M-1 as a column vector
   x:      current state at time k (now)
   Ts:     controller sample time
   N:      prediction horizon
   M:      control horizon
   xref:   state references as a matrix whose N+1(from instant 0 to N) rows are the reference for the system

   u1:     previous controller output at time k-1
   J:      objective function cost

 NonlinearTs MPC design parameters
 */

      //Q matrix penalizes state deviations from the references.
      Eigen::DiagonalMatrix<double, 3> Q(10, 10, 1);

      //terminal cost matrix
      Eigen::DiagonalMatrix<double, 3> P = 100 * Q;

      // Rdu matrix penalizes the input rate of change (du).
      Eigen::DiagonalMatrix<double, 2> Rdu(2, 20);

      // Ru matrix penalizes the input magnitude
      Eigen::DiagonalMatrix<double, 2> Ru(3, 3);

      //init ref_k
      Eigen::VectorXd ref_k(3);
      ref_k << 0, 0, 0;

      //init theta_d
      double theta_d = 0.0;

      //init ek
      Eigen::VectorXd ek(3);
      ek << 0, 0, 0;

      // reshape du to a matrix which rows are du[k] from k to k+N, filling
      // rows after M with zeros
      Eigen::MatrixXd alldu(N, 2);
      alldu.setZero(N, 2);
      int w = 0;
      for (int i = 0; i < M; ++i)
      {
        for (int c = 0; c < 2; ++c)
        {
          alldu(i, c) = du(w);
          w++;
        }
      }

      /*
  Cost Calculation
  Set initial plant states and cost.
  */

      //xk = x0;
      Eigen::VectorXd xk(3);
      xk << 0, 0, 0;
      for (int i = 0; i < 3; i++)
      {
        xk(i) = x0(i);
      }

      //uk = u1;
      Eigen::VectorXd uk(2);
      uk << 0, 0;
      for (int i = 0; i < 2; i++)
      {
        uk(i) = u1(i);
      }

      //init J
      double j = 0;

      for (int i = 0; i < N; i++)
      {
        Eigen::VectorXd du_k(2);
        du_k << 0, 0;
        for (int j = 0; j < 2; j++)
        {
          du_k(j) = alldu(i, j);
        }
        du_k = du_k.transpose();
        uk = uk + du_k;

        for (int j = 0; j < 3; j++)
        {
          ref_k(j) = xref(i, j);
        }
        ref_k = ref_k.transpose();
       
        theta_d = ref_k(2);

        //Rot
        Eigen::MatrixXd Rot(3, 3);
        Rot.setZero(3, 3);
        
        Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;

        
        ek = Rot * (xk - ref_k);

        //std::cout<<"errooooooooooooooore"<<std::endl;
        //std::cout<<"xk="<<xk<<std::endl;
        //std::cout<<"ref_k="<<ref_k<<std::endl;
       // std::cout<<"errooooooooooooooore"<<std::endl;

        j = j + ek.transpose() * Q * ek;
        j = j + du_k.transpose() * Rdu * du_k;
        j = j + uk.transpose() * Ru * uk;

        //obtain plant state at next prediction step
        xk = mobRobDT0(xk, uk, Ts);
      }
      //terminal cost
      for (int j = 0; j < 3; j++)
      {
        ref_k(j) = xref(N, j);
      }

      ref_k = ref_k.transpose();

      theta_d = ref_k(2);

      Eigen::MatrixXd Rot(3, 3);
      Rot.setZero(3, 3);

      Rot << cos(theta_d), sin(theta_d), 0, (-sin(theta_d)), cos(theta_d), 0, 0, 0, 1;
      ek = Rot * (xk - ref_k);

      j = j + ek.transpose() * P * ek;

      //output J
      //std::cout << "J = " <<std::to_string(j) << std::endl;

      return j;
    };

    double FunzioneDiCosto(Eigen::VectorXd du)
    {

      int M = 3;
      int N = 15;

      

      Eigen::VectorXd x0(3);
      x0<<G_posizione_iniziale[0],G_posizione_iniziale[1],G_posizione_iniziale[2];
    

      double Ts = 0.1;

      Eigen::MatrixXd xref(N + 1, 3);
      xref.setZero(N + 1, 3);

    //aggiungo ref !=0
      for (int i = 0; i < N+1; ++i)
      {
        
        for (int c = 0; c < 3; ++c)
        {
          if(ScorriDPxk+c+3*i<=(DPxk.size()-3)){
            xref(i,c)=DPxk(ScorriDPxk+c+3*i);
          } else {
            xref(i,c)=DPxk((DPxk.size()-3)+c);

          }
         
        }
      }
      
      /*std::cout<<"reeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef"<<std::endl;
      std::cout<<xref<<std::endl;

      std::cout<<"reeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeef"<<std::endl;*/
      
      Eigen::VectorXd u1(2);
       u1<<G_vel_precedente[0],G_vel_precedente[1];

      double j = mobRobCostFCN(du, x0, Ts, N, M, xref, u1);
      return j;
    };

    // fine funzione di costo

    
